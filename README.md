# GPIO Scheduler
A simple backend for managing 'entities' and setting schedules for the these entities to be used against mqtt or 
raspberrypi depending on configuration.

## Install gpioscheduler
* $ go get -u gitlab.com/peterbradford/gpioscheduler

## Running the service
* create a .env and fill in the necessary values

## MQTT
whatever id is set will be used as a topic at the end of the provided topic
e.g. id = 0 will write start/stop commands to <-topic from config->/0

## Raspberry pi pins
Id 0 will 
map to the corresponding BCM 4, which maps to the physical pin 7. 
this is mostly due to the library being used for gpio pin control which uses BCM values. 
see diagram below for all mappings

```
Id --> BCM -> Physical
0  ---->  4 --> 7
1  ---->  5 --> 29
2  ---->  6 --> 31 
3  ----> 12 --> 32
4  ----> 13 --> 33
5  ----> 16 --> 36
6  ----> 17 --> 11
7  ----> 18 --> 12
8  ----> 19 --> 35
9 ----> 20 --> 38
10 ----> 21 --> 40
11 ----> 22 --> 15
12 ----> 23 --> 16
13 ----> 24 --> 18
14 ----> 25 --> 22
15 ----> 27 --> 13
```

#### This project is primarily used for scheduling gpios. The best example is a sprinkler system. You can set weekly schedules that reoccure every week or use cron to set a schedule. I've used this repo (https://github.com/robfig/cron) to handle the parsing of cron commands and to handle running the necessary funcs at the correct time
This is a helpful website to test your crons against and get ideas using the random selection that it offers 
https://crontab.guru/







{
"1": {
"id": 1,
"name": "front yard",
"status": false,
"enabled": true,
"info": "",
"crons": {},
"weekly": {
"fri 05:25": 24,
"sun 05:25": 25,
"tue 05:25": 25
},
"next": "tue 05:25",
"nextDur": 25
},
"2": {
"id": 2,
"name": "strip",
"status": false,
"enabled": true,
"info": "",
"crons": {},
"weekly": {
"fri 06:00": 20,
"sun 06:00": 20,
"tue 06:00": 20
},
"next": "tue 06:00",
"nextDur": 20
},
"3": {
"id": 3,
"name": "flowers",
"status": false,
"enabled": true,
"info": "",
"crons": {},
"weekly": {
"fri 08:00": 15,
"mon 08:00": 15,
"wed 08:00": 15
},
"next": "wed 08:00",
"nextDur": 15
},
"4": {
"id": 4,
"name": "backyard east",
"status": false,
"enabled": true,
"info": "",
"crons": {},
"weekly": {
"fri 06:30": 10,
"sun 06:30": 9,
"tue 06:30": 10
},
"next": "tue 06:30",
"nextDur": 10
},
"5": {
"id": 5,
"name": "backyard west",
"status": false,
"enabled": true,
"info": "",
"crons": {},
"weekly": {
"fri 06:50": 10,
"sun 06:46": 9,
"tue 06:50": 10
},
"next": "tue 06:50",
"nextDur": 10
}
}
