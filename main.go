package main

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/peterbradford/gpioscheduler/internal/api"
	"gitlab.com/peterbradford/gpioscheduler/internal/config"
	"gitlab.com/peterbradford/gpioscheduler/internal/cron"
	database "gitlab.com/peterbradford/gpioscheduler/internal/db"
	"gitlab.com/peterbradford/gpioscheduler/internal/entitymanager"
	"gitlab.com/peterbradford/gpioscheduler/internal/hardware"
	"gitlab.com/peterbradford/gpioscheduler/internal/logger"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

type GPIOScheduler struct {
	closeSigChan chan os.Signal

	wg     *sync.WaitGroup
	config *config.Config
}

type ExitCode int

const (
	Ok ExitCode = iota
	LoggerFileError
	MQTTError
	RpiError
	APIError
	ConfigError
	DBError
)

var app GPIOScheduler

func init() {
	c := config.InitConfig()

	app = GPIOScheduler{
		wg:           &sync.WaitGroup{},
		config:       c,
		closeSigChan: make(chan os.Signal, 1),
	}

	//logger.Init(zerolog.Level(c.LogLevel))
	logger.InitPretty(zerolog.Level(c.LogLevel))

	if c.LogPath != "" {
		if err := logger.InitFile(c.LogPath); err != nil {
			os.Exit(int(LoggerFileError))
		}
	}

}

func (app *GPIOScheduler) run() (ExitCode, error) {
	apiErrChan := make(chan error)
	signal.Notify(app.closeSigChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	var h hardware.Hardware
	var err error

	switch app.config.HardwareType {
	case config.Mqtt:
		h, err = hardware.NewMQTT(app.config)
		if err != nil {
			return MQTTError, err
		}
	case config.Rpi:
		h, err = hardware.NewRpi(app.config)
		if err != nil {
			return RpiError, err
		}
	case config.Test:
		h = hardware.NewTester()
	default:
		log.Error().Msgf("hardware type not set in config or invalid")
		return ConfigError, nil
	}

	var db database.DB
	switch app.config.DBType {
	case config.File:
		db, err = database.NewFile(app.config.DBPath)
		if err != nil {
			return DBError, err
		}
	default:
		err = fmt.Errorf("dbtype '%v' is invalid", app.config.DBType)
		log.Err(err).Msg("")
		return ConfigError, err
	}

	c := cron.New(app.config.Location)

	manager := entitymanager.New(app.config, apiErrChan, h, db, c)

	a := api.New(app.wg, app.config, apiErrChan, manager)

	//run internal
	a.Run()

	var rtnCode ExitCode
	select {
	case <-app.closeSigChan:
		log.Info().Msgf("received a shutdown signal")
		rtnCode = Ok
	case err = <-apiErrChan:
		log.Error().Msgf("shutting down with api error: %v", err)
		rtnCode = APIError
	}

	a.Stop()

	close(app.closeSigChan)

	return rtnCode, nil
}

func main() {
	//todo should we handle schedules overlapping, e.g. 12:00 for 30 minutes and 12:15 for 20 minutes.
	log.Info().Msg("starting scheduler")
	if code, err := app.run(); err != nil {
		log.Error().Err(err).Msg("")
		os.Exit(int(code))
	}
	app.wg.Wait()
	_ = logger.CloseLogFile()
	log.Info().Msg("stopped scheduler successfully")
}
