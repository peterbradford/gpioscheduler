package cron

import (
	"github.com/robfig/cron/v3"
	"github.com/rs/zerolog/log"
	"sync"
	"time"
)

type CronJob struct {
	CronId   cron.EntryID `json:"cronId"`
	Duration int          `json:"duration"`
}

type Job struct {
	ID   int       `json:"id"`
	Next time.Time `json:"next"`
	Prev time.Time `json:"prev"`
}

type Cron struct {
	cron *cron.Cron
}

var (
	inst *Cron
	once sync.Once
)

func New(timeZone *time.Location) *Cron {
	once.Do(func() {
		inst = &Cron{cron: cron.New(cron.WithLocation(timeZone))}
		inst.cron.Start()
		log.Info().Msgf("cron running in: %v", inst.cron.Location())
	})
	return inst
}

func (c *Cron) Stop() {
	c.cron.Stop()
}

func (c *Cron) Add(sched string, cmd func()) (*cron.EntryID, error) {
	log.Trace().Msgf("adding '%v' to cron", sched)
	id, err := c.cron.AddFunc(sched, cmd)
	if err != nil {
		log.Err(err).Msgf("error while adding '%v' to cron", sched)
		return nil, err
	}
	return &id, nil
}

func (c *Cron) Remove(id cron.EntryID) {
	log.Trace().Msgf("removing schedule from cron: %v", id)
	c.cron.Remove(id)
}

func (c *Cron) GetNext(id cron.EntryID) time.Time {
	return c.cron.Entry(id).Next
}

//func (c Cron) GetCronJob(id cron.EntryID) cron.Entry {
//	return c.cron.Entry(id)
//}

//func (c Cron) GetOneWeek(id cron.EntryID, loc *time.Location) []*time.Time {
//	now := time.Date(time.Now().In(loc).Year(), time.Now().In(loc).Month(), time.Now().In(loc).Day(), 0, 0, 0, 0, loc)
//	weekStart := now.AddDate(0, 0, -int(now.Weekday()))
//	weekEnd := weekStart.AddDate(0, 0, 7)
//	weekStart = weekStart.Add(-time.Minute)
//
//	rslts := []*time.Time{}
//	for n := c.cron.Entry(id).Schedule.Next(weekStart); n.Before(weekEnd); n = c.cron.Entry(id).Schedule.Next(n) {
//		nt := n
//		rslts = append(rslts, &nt)
//	}
//	return rslts
//}

func (c *Cron) GetCronJobs() []Job {
	log.Trace().Msgf("cron jobs: %v", c.cron.Entries())
	var jobs []Job
	for _, e := range c.cron.Entries() {
		jobs = append(jobs, Job{ID: int(e.ID), Next: e.Next, Prev: e.Prev})
	}

	return jobs
}
