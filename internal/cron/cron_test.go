package cron

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestLocation(t *testing.T) {
	l, err := time.LoadLocation("America/Denver")
	assert.NoError(t, err)
	_ = New(l)
}

func TestAdd1(t *testing.T) {
	cc := New(time.UTC)

	tmp := 0
	_, err := cc.Add("* * * * *", func() {
		tmp = 1
	})
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Minute)
	if tmp != 1 {
		t.Error("0 expected 1")
	}
}

func TestRemove(t *testing.T) {
	cc := New(time.UTC)

	tmp := 0
	id, err := cc.Add("* * * * *", func() {
		tmp += 1
	})
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Minute)
	assert.Equal(t, 1, tmp)
	cc.Remove(*id)
	time.Sleep(time.Minute + (time.Second * 2))
	assert.Equal(t, 1, tmp)
}

func TestNext(t *testing.T) {
	cc := New(time.UTC)

	tmp := 0
	id, err := cc.Add("* * * * *", func() {
		tmp += 1
	})
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, time.Now().Minute()+1, cc.GetNext(*id).Minute())
}

func TestStop(t *testing.T) {
	cc := New(time.UTC)

	tmp := 0
	_, err := cc.Add("* * * * *", func() {
		tmp += 1
	})
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Minute)
	assert.Equal(t, 1, tmp)
	cc.Stop()
	time.Sleep(time.Minute + (time.Second * 2))
	assert.Equal(t, 1, tmp)
}

//func TestGet1Week(t *testing.T) {
//	cc := GetAll()
//	go cc.Run()
//
//	id, err := cc.Add("* * * * *", func() {})
//	if err != nil {
//		t.Error(err)
//	}
//	cc.GetOneWeek(id, time.UTC)
//}
