package db

import "gitlab.com/peterbradford/gpioscheduler/internal/entity"

type DB interface {
	GetAll() (map[int]*entity.Entity, error)
	Get(int) (*entity.Entity, error)
	Update(*entity.Entity) error
	UpdateAll(map[int]*entity.Entity) error
	Close() error
}
