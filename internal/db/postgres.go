package db

import (
	"errors"
	"gitlab.com/peterbradford/gpioscheduler/internal/entity"
)

// interface guard
var _ DB = (*Postgres)(nil)

type Postgres struct{}

func (pg *Postgres) Get(i int) (*entity.Entity, error) {
	//TODO implement me
	return nil, errors.New("IMPLEMENT ME")
}

func (pg *Postgres) Update(e *entity.Entity) error {
	//TODO implement me
	return errors.New("IMPLEMENT ME")
}

func (pg *Postgres) UpdateAll(m map[int]*entity.Entity) error {
	//TODO implement me
	return errors.New("IMPLEMENT ME")
}

func (pg *Postgres) Close() error {
	//TODO implement me
	return errors.New("IMPLEMENT ME")
}

func (pg *Postgres) GetAll() (map[int]*entity.Entity, error) {
	return nil, errors.New("IMPLEMENT ME")
}

func (pg *Postgres) Save(entities map[int]*entity.Entity) error {
	return errors.New("IMPLEMENT ME")
}
