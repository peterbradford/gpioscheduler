package db

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/peterbradford/gpioscheduler/internal/entity"
	"io"
	"os"
)

// interface guard
var _ DB = (*File)(nil)

type File struct {
	file *os.File
}

func NewFile(path string) (*File, error) {
	//fullFilePath := fmt.Sprintf("%s/%s", path, "db.json")
	file, err := checkIfFileExistsOrCreate(path)
	if err != nil {
		return nil, err
	}

	return &File{
		file: file,
	}, nil
}

func (f *File) Get(id int) (*entity.Entity, error) {
	//entities, err := f.GetAll()
	//if err != nil {
	//	return nil, err
	//}
	//
	//if v, ok := entities[id]; !ok {
	//	err = fmt.Errorf("entity with id %v doesn't exist", id)
	//	log.Err(err).Msg("")
	//	return nil, err
	//} else {
	//	return v, nil
	//}
	return nil, errors.New("implement me!")
}

func (f *File) GetAll() (map[int]*entity.Entity, error) {
	log.Trace().Msg("loading entities from file")

	byteValue, err := io.ReadAll(f.file)
	if err != nil {
		log.Err(err).Msg("unable to read db file")
		return nil, err
	}

	var fileEntities map[int]*entity.Entity

	if err = json.Unmarshal(byteValue, &fileEntities); err != nil {
		log.Err(err).Msg("unable to load entities from db file")
		return nil, err
	}

	return fileEntities, nil
}

func (f *File) UpdateAll(entities map[int]*entity.Entity) error {

	data, err := json.MarshalIndent(entities, "", " ")
	if err != nil {
		log.Err(err).Msg("unable to marshal entities")
		return err
	}

	if err = f.file.Truncate(0); err != nil {
		log.Err(err).Msg("unable to truncate the db file")
		return err
	}

	if _, err = f.file.WriteAt(data, 0); err != nil {
		log.Err(err).Msg("unable to write to db file")
		return err
	}
	return nil
}

func (f *File) Update(e *entity.Entity) error {
	entities, err := f.GetAll()
	if err != nil {
		return err
	}

	entities[e.Id] = e

	err = f.UpdateAll(entities)
	if err != nil {
		return err
	}

	return nil
}

func (f *File) Close() error {
	if err := f.file.Close(); err != nil {
		log.Warn().Msg("unable to close db file")
		return err
	}
	return nil
}

func checkIfFileExistsOrCreate(path string) (*os.File, error) {
	fullFilePath := fmt.Sprintf("%s/%s", path, "db.json")
	if _, err := os.Stat(fullFilePath); os.IsNotExist(err) {
		err = os.MkdirAll(path, 0700)
		if err != nil {
			log.Err(err).Msgf("unable to open db file: %v", path)
			return nil, err
		}

		if err = os.WriteFile(fullFilePath, []byte("{}"), 0644); err != nil {
			log.Err(err).Msgf("unable to write empty db file: %v", path)
			return nil, err
		}
	}
	file, err := os.OpenFile(fullFilePath, os.O_RDWR, 0600)
	if err != nil {
		log.Err(err).Msgf("unable to open db file: %v", path)
		return nil, err
	}
	return file, nil
}
