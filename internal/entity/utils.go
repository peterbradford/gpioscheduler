package entity

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	sortableTime "gitlab.com/peterbradford/gpioscheduler/internal/time"
	"strings"
)

func convertToLowercaseThreeLetterDay(s string) (string, error) {
	split := strings.Split(strings.Trim(s, "\""), " ")

	if len(split) != 2 {
		log.Error().Msgf("bad input to convert to day '%v', expected 'sun 15:30' or similar", s)
		return "", errors.New(fmt.Sprintf("bad input to convert to day '%v', expected 'sun 15:30' or similar", s))
	}

	day := ""
	switch strings.ToLower(split[0]) {
	case "sun", "sunday":
		day = "sun"
	case "mon", "monday":
		day = "mon"
	case "tue", "tues", "tuesday":
		day = "tue"
	case "wed", "wednesday":
		day = "wed"
	case "thu", "thur", "thursday":
		day = "thu"
	case "fri", "friday":
		day = "fri"
	case "sat", "saturday":
		day = "sat"
	default:
		log.Error().Msgf("bad day name '%v' in weekly schedule", split[0])
		return "", errors.New(fmt.Sprintf("bad day name '%v' in weekly schedule", split[0]))
	}
	return day + " " + split[1], nil
}

func normalizeWeeklyDayInput(m map[string]int) (map[string]int, error) {
	newMap := map[string]int{}
	for k, _ := range m {
		newDay, err := convertToLowercaseThreeLetterDay(k)
		if err != nil {
			return nil, err
		}
		newMap[newDay] = m[k]
	}
	return newMap, nil
}

func convertWeeklyKeyToCron(weekly string) (string, error) {
	t, err := sortableTime.New(weekly)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%v %v * * %v", t.Minute(), t.Hour(), int(t.Weekday())), nil
}
