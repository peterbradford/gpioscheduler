package entity

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConvertToThreeLetterDay(t *testing.T) {
	_, err := convertToLowercaseThreeLetterDay("badstring")
	assert.Error(t, err, "expecting error from badstring")

	_, err = convertToLowercaseThreeLetterDay("bad string")
	assert.Error(t, err, "expecting error from badstring")

	s, err := convertToLowercaseThreeLetterDay("SUNDAY 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "sun 15:30", s, fmt.Sprintf("expecting 'sun' got %v", s))

	s, err = convertToLowercaseThreeLetterDay("MONDAY 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "mon 15:30", s, fmt.Sprintf("expecting 'mon' got %v", s))

	s, err = convertToLowercaseThreeLetterDay("tuesday 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "tue 15:30", s, fmt.Sprintf("expecting 'tue' got %v", s))

	s, err = convertToLowercaseThreeLetterDay("wednesday 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "wed 15:30", s, fmt.Sprintf("expecting 'wed' got %v", s))

	s, err = convertToLowercaseThreeLetterDay("thur 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "thu 15:30", s, fmt.Sprintf("expecting 'thu' got %v", s))

	s, err = convertToLowercaseThreeLetterDay("frIDay 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "fri 15:30", s, fmt.Sprintf("expecting 'fri' got %v", s))

	s, err = convertToLowercaseThreeLetterDay("SatuRday 15:30")
	assert.NoError(t, err, "asserting no error from good string")
	assert.Equal(t, "sat 15:30", s, fmt.Sprintf("expecting 'sat' got %v", s))
}

func TestConvertAllToThreeLetterDays(t *testing.T) {

	_, err := normalizeWeeklyDayInput(map[string]int{"badstring": 1})
	assert.Error(t, err, "asserting error from bad string")

	_, err = normalizeWeeklyDayInput(map[string]int{"sunday 15:30": 1})
	assert.NoError(t, err, "asserting no error from 'sun 15:30'")

	r, err := normalizeWeeklyDayInput(map[string]int{"sunday 15:30": 1, "monDay 15:30": 2})
	assert.Equal(t, map[string]int{"sun 15:30": 1, "mon 15:30": 2}, r)
}

func TestConvertWeeklyKeyToCron(t *testing.T) {

	_, err := convertWeeklyKeyToCron("something")
	assert.Error(t, err, "asserting error from bad string 'something'")

	s, err := convertWeeklyKeyToCron("sun 15:30")
	assert.NoError(t, err, "asserting no error from string 'sun 15:30'")
	assert.Equal(t, "30 15 * * 0", s)
}
