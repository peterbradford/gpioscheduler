package entity

import (
	"context"
	"errors"
	"sort"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/peterbradford/gpioscheduler/internal/cron"
	"gitlab.com/peterbradford/gpioscheduler/internal/hardware"
	sortableTime "gitlab.com/peterbradford/gpioscheduler/internal/time"
)

type RunningStats struct {
	Remaining       time.Duration `json:"remaining"`
	RemainingStr    string        `json:"remainingStr"`
	PercentComplete float32       `json:"percentComplete"`
}

type Entity struct {
	//internals
	readChan      chan *RunningStats
	sendStatsChan chan interface{}
	cronJobs      map[string]*cron.CronJob //key will be either 'fri 01:00' or '0 0 0 * *'
	runner        hardware.Hardware
	running       bool
	cron          *cron.Cron

	//json out and api/file read ins
	Id      int            `json:"id"`
	Name    string         `json:"name"`
	Status  bool           `json:"status"`
	Enabled bool           `json:"enabled"`
	Info    string         `json:"info"`
	Crons   map[string]int `json:"crons"`  //these are cron schedules coming from the file/api, key ~= "* * * * *"
	Weekly  map[string]int `json:"weekly"` //these are weekly schedules comes from the file/api, key ~= "mon 11:11"
	Next    string         `json:"next"`
	NextDur int            `json:"nextDur"`
}

func New(id int, name, info string, crons, weekly map[string]int, hardware hardware.Hardware, c *cron.Cron) (*Entity, error) {
	if crons == nil {
		crons = map[string]int{}
	}
	if weekly == nil {
		weekly = map[string]int{}
	}

	e := &Entity{
		Id:      id,
		Name:    name,
		Status:  false,
		Enabled: true,
		Info:    info,
		Crons:   crons,
		Weekly:  weekly,

		readChan:      make(chan *RunningStats, 1),
		sendStatsChan: make(chan interface{}, 1),
		cronJobs:      make(map[string]*cron.CronJob),
		runner:        hardware,
		running:       false,
		cron:          c,
	}

	if err := e.cleanAndLoadWeekly(); err != nil {
		return nil, err
	}

	if err := e.cleanAndLoadCron(); err != nil {
		return nil, err
	}

	e.updateNextAndNextDur()

	return e, nil
}

func (e *Entity) cleanAndLoadWeekly() error {
	var err error
	e.Weekly, err = normalizeWeeklyDayInput(e.Weekly)
	if err != nil {
		return err
	}

	for k, v := range e.Weekly {
		cronString, err := convertWeeklyKeyToCron(k)
		if err != nil {
			return err
		}
		if err := e.AddToCron(cronString, k, v, e.cron); err != nil {
			log.Err(err).Msgf("error while adding weekly schedule %v to cron", k)
			return err
		}
	}
	return nil
}

func (e *Entity) cleanAndLoadCron() error {
	for k, v := range e.Crons {
		if err := e.AddToCron(k, k, v, e.cron); err != nil {
			return err
		}
	}
	return nil
}

func (e *Entity) Enable() {
	log.Info().Msgf("enabling entity %v", e.Id)
	e.Enabled = true
}

func (e *Entity) Disable() {
	log.Info().Msgf("disabling entity %v", e.Id)
	e.Enabled = false
}

func (e *Entity) Update(new *Entity) error {
	var err error
	if new == nil {
		err = errors.New("update received a nil pointer")
		log.Err(err).Msg("")
		return err
	}
	log.Trace().Msgf("updating entity %v", new.Id)

	e.printJsonParts("old")

	e.Name = new.Name
	e.Info = new.Info
	e.Enabled = new.Enabled

	if new.Weekly == nil {
		new.Weekly = map[string]int{}
	}

	if new.Crons == nil {
		new.Crons = map[string]int{}
	}

	new.Weekly, err = normalizeWeeklyDayInput(new.Weekly)
	if err != nil {
		return err
	}

	if err := e.updateWeekly(new.Weekly); err != nil {
		return err
	}

	e.updateCrons(new.Crons)

	e.updateNextAndNextDur()

	e.printJsonParts("new")

	return nil
}

func (e *Entity) GetRemainingTime(ctx context.Context) *RunningStats {
	e.sendStatsChan <- ""
	select {
	case <-ctx.Done():
		return nil
	case v := <-e.readChan:
		return v
	}
}

func (e *Entity) AddToCron(cronString, key string, dur int, c *cron.Cron) error {
	if c == nil {
		return errors.New("nil cron passed in")
	}

	if id, err := c.Add(cronString, e.RunEntityForDurFuncCreator(dur)); err != nil {
		return err
	} else {
		e.cronJobs[key] = &cron.CronJob{CronId: *id, Duration: dur}
	}
	return nil
}

func (e *Entity) RunEntityForDurFuncCreator(duration int) func() {
	newDur := duration
	return func() {
		endTime := time.Now().Add(time.Duration(newDur) * time.Minute)
		timer := time.NewTimer(time.Duration(newDur) * time.Minute)
		if e.Enabled {
			log.Info().Msgf("running %v for %v minute(s)", e.Id, newDur)
			e.runner.TurnOn(e.Id)
			e.running = true
			e.Status = true
		Running:
			for {
				select {
				case <-timer.C:
					break Running
				case <-e.sendStatsChan:
					remaining := time.Until(endTime)
					percent := ((float32(newDur*60) - float32(remaining/time.Second)) / float32(newDur*60)) * 100
					e.readChan <- &RunningStats{Remaining: remaining / time.Millisecond, RemainingStr: remaining.String(), PercentComplete: percent}
				}
			}

			e.runner.TurnOff(e.Id)
			e.running = false
			e.Status = false
		} else {
			log.Debug().Msgf("%v disabled, skipping", e.Id)
		}
	}
}

func (e *Entity) TurnOn() {
	e.Status = true
	e.runner.TurnOn(e.Id)
}

func (e *Entity) TurnOff() {
	e.Status = false
	e.runner.TurnOff(e.Id)
}

func (e *Entity) updateWeekly(new map[string]int) error {
	//iterate over weekly map
	for k, oldDur := range e.Weekly {
		//check if weekly k exist in new weekly map
		if _, ok := new[k]; !ok {
			//remove from cron
			e.cron.Remove(e.cronJobs[k].CronId)
			//delete from Weekly and cronjobs map
			delete(e.cronJobs, k)
			delete(e.Weekly, k)
		}

		if newDur, ok := new[k]; ok && oldDur != newDur {
			//remove from cron
			e.cron.Remove(e.cronJobs[k].CronId)
			//delete from Weekly and cronjobs map
			delete(e.cronJobs, k)
			delete(e.Weekly, k)
		}
	}

	//iterate over new Weekly map
	for k, v := range new {
		//check if the new Weekly k already exist in the Weekly map
		if _, ok := e.Weekly[k]; !ok {

			cronString, err := convertWeeklyKeyToCron(k)
			if err != nil {
				return err
			}

			e.Weekly[k] = v

			if err := e.AddToCron(cronString, k, v, e.cron); err != nil {

				log.Err(err).Msgf("error adding weekly %v to cron", k)
			}
		}
	}
	return nil
}

func (e *Entity) updateCrons(new map[string]int) {
	//iterate over Crons map
	for k, oldDur := range e.Crons {
		//check if cron k exist in new crons map
		if _, ok := new[k]; !ok {
			//remove from cron
			e.cron.Remove(e.cronJobs[k].CronId)
			//delete from Crons map
			delete(e.cronJobs, k)
			delete(e.Crons, k)

		}

		//if schedule/cron is the same but duration is different need to remove
		if newDur, ok := new[k]; ok && oldDur != newDur {
			//remove from cron
			e.cron.Remove(e.cronJobs[k].CronId)
			//delete from Weekly and cronjobs map
			delete(e.cronJobs, k)
			delete(e.Crons, k)
		}
	}

	//iterate over new Crons map
	for k, v := range new {
		//check that the new cron doesn't already exist in the Crons map
		if _, ok := e.Crons[k]; !ok {
			//if it doesn't, add it to the map
			e.Crons[k] = v
			//add it to cron
			if err := e.AddToCron(k, k, v, e.cron); err != nil {
				log.Err(err).Msgf("error adding cron %v to cron", k)
			}
		}
	}
}

func (e *Entity) RemoveAllCronJobs() {
	for _, v := range e.cronJobs {
		e.cron.Remove(v.CronId)
	}
}

func (e *Entity) updateNextAndNextDur() {
	var schedules sortableTime.Times
	for _, v := range e.cronJobs {
		schedules = append(schedules, e.cron.GetNext(v.CronId))
	}
	sort.Sort(schedules)
	if len(schedules) == 0 {
		e.Next = ""
		e.NextDur = -1
	} else {
		e.Next = strings.ToLower(schedules[0].Format(sortableTime.MainFormat))

		cronKey, err := convertWeeklyKeyToCron(e.Next)
		if err != nil {
			return
		}

		if _, ok := e.cronJobs[strings.ToLower(e.Next)]; ok {
			e.NextDur = e.cronJobs[strings.ToLower(e.Next)].Duration
		} else if _, ok = e.cronJobs[cronKey]; ok {
			e.NextDur = e.cronJobs[cronKey].Duration
		} else {
			log.Error().Msgf("cronJob not found with key '%v' or '%v' in %v", e.Next, schedules[0].Format(sortableTime.MainFormat), e.cronJobs)
			return
		}
	}
}

func (e *Entity) printJsonParts(prefix string) {
	log.Trace().Msgf("%v    id:%v name:%v status:%v enabled:%v info:%v crons:%v weekly:%v next:%v cronJobs:%v", prefix, e.Id, e.Name, e.Status, e.Enabled, e.Info, e.Crons, e.Weekly, e.Next, e.cronJobs)
}
