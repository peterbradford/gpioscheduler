package entity

import (
	"errors"
	"fmt"
	cron2 "github.com/robfig/cron/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.com/peterbradford/gpioscheduler/internal/cron"
	"gitlab.com/peterbradford/gpioscheduler/internal/hardware"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	c := cron.New(time.UTC)
	for _, v := range []struct {
		testName    string
		id          int
		name        string
		info        string
		crons       map[string]int
		weekly      map[string]int
		next        string
		nextDur     int
		expected    *Entity
		expectedErr error
	}{
		{
			"nils",
			0,
			"new name",
			"new info",
			nil,
			nil,
			"",
			-1,
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			nil,
		},
		{
			"empties",
			0,
			"new name",
			"new info",
			map[string]int{},
			map[string]int{},
			"",
			-1,
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			nil,
		},
		{
			"bad weekly",
			0,
			"new name",
			"new info",
			map[string]int{},
			map[string]int{"bad string": 1},
			"",
			-1,
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			errors.New("bad day name 'bad' in weekly schedule"),
		},
		{
			"single weekly",
			0,
			"new name",
			"new info",
			map[string]int{},
			map[string]int{"sunday 15:30": 30},
			"",
			-1,
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{"sun 15:30": 30},
				Enabled: true,
				Next:    "sun 15:30",
				NextDur: 30,
			},
			nil,
		},
		{
			"bad cron",
			0,
			"new name",
			"new info",
			map[string]int{"badcron": 30},
			map[string]int{},
			"",
			-1,
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			errors.New("expected exactly 5 fields, found 1: [badcron]"),
		},
		{
			"one cron",
			0,
			"new name",
			"new info",
			map[string]int{"30 5 * * 0": 30},
			map[string]int{},
			"",
			-1,
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{"30 5 * * 0": 30}, //05:30 on sunday
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "sun 05:30",
				NextDur: 30,
			},
			nil,
		},
	} {
		e, err := New(v.id, v.name, v.info, v.crons, v.weekly, &hardware.Tester{}, c)
		if err != nil {
			assert.Equal(t, v.expectedErr, err, fmt.Sprintf("%v.%v", v.testName, "expectedErr"))
			continue
		}

		assert.Equal(t, v.expected.Id, e.Id, fmt.Sprintf("%v.%v", v.testName, "id"))
		assert.Equal(t, v.expected.Name, e.Name, fmt.Sprintf("%v.%v", v.testName, "name"))
		assert.Equal(t, v.expected.Status, e.Status, fmt.Sprintf("%v.%v", v.testName, "status"))
		assert.Equal(t, v.expected.Enabled, e.Enabled, fmt.Sprintf("%v.%v", v.testName, "enabled"))
		assert.Equal(t, v.expected.Info, e.Info, fmt.Sprintf("%v.%v", v.testName, "info"))
		assert.Equal(t, v.expected.Weekly, e.Weekly, fmt.Sprintf("%v.%v", v.testName, "weekly"))
		assert.Equal(t, v.expected.Crons, e.Crons, fmt.Sprintf("%v.%v", v.testName, "crons"))
		assert.Equal(t, v.expected.Next, e.Next, fmt.Sprintf("%v.%v", v.testName, "next"))
		assert.Equal(t, v.expected.NextDur, e.NextDur, fmt.Sprintf("%v.%v", v.testName, "nextDur"))
		assert.Equal(t, len(v.crons)+len(v.weekly), len(e.cronJobs), fmt.Sprintf("%v.%v", v.testName, "cronJobs"))
	}
}

func TestEnable(t *testing.T) {
	c := cron.New(time.UTC)
	e, err := New(0, "test-name", "test-info", nil, nil, nil, c)
	assert.Equal(t, nil, err, fmt.Sprintf("%v.%v", "enable", "expectedErr"))

	e.Enable()

	assert.True(t, e.Enabled, "enabled")
}

func TestDisable(t *testing.T) {
	c := cron.New(time.UTC)
	e, err := New(0, "test-name", "test-info", nil, nil, nil, c)
	assert.Equal(t, nil, err, fmt.Sprintf("%v.%v", "disable", "expectedErr"))

	e.Disable()

	assert.True(t, !e.Enabled, "enabled")
}

func TestTurnOn(t *testing.T) {
	c := cron.New(time.UTC)
	runner := &hardware.Tester{}
	e, err := New(0, "test-name", "test-info", nil, nil, runner, c)
	assert.Equal(t, nil, err, fmt.Sprintf("%v.%v", "enable", "expectedErr"))

	e.TurnOn()

	assert.True(t, e.Status, "on")
}

func TestTurnOff(t *testing.T) {
	c := cron.New(time.UTC)
	runner := &hardware.Tester{}
	e, err := New(0, "test-name", "test-info", nil, nil, runner, c)
	assert.Equal(t, nil, err, fmt.Sprintf("%v.%v", "enable", "expectedErr"))

	e.TurnOff()

	assert.False(t, e.Status, "off")
}

func TestRemoveAllCronJobs(t *testing.T) {
	c := cron.New(time.UTC)
	cronJobs := c.GetCronJobs()
	for _, v := range cronJobs {
		c.Remove(cron2.EntryID(v.ID))
	}
	runner := &hardware.Tester{}
	e, err := New(0, "test-name", "test-info", nil, map[string]int{"sun 12:00": 1, "sun 12:05": 1, "sun 12:10": 1, "sun 12:15": 1, "sun 12:20": 1}, runner, c)
	assert.Equal(t, nil, err, fmt.Sprintf("%v.%v", "enable", "expectedErr"))

	assert.Equal(t, 5, len(c.GetCronJobs()), "expecting 5 cron jobs")

	e.RemoveAllCronJobs()

	assert.Equal(t, 0, len(c.GetCronJobs()), "expecting no cron jobs left")

	//todo should probably check cron and make sure only the specific crons are removed
}

func TestUpdateWeekly(t *testing.T) {
	c := cron.New(time.UTC)
	for _, v := range []struct {
		testName    string
		entity      *Entity
		newWeekly   map[string]int
		expectedErr error
	}{
		{
			testName: "empty",
			entity: &Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			newWeekly:   map[string]int{},
			expectedErr: nil,
		},
		{
			testName: "add 1 bad to empty",
			entity: &Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			newWeekly:   map[string]int{"badstring": 30},
			expectedErr: errors.New("expecting '<day> <hour>:<minute>' gotbadstring"),
		},
		{
			testName: "add 1 good to empty",
			entity: &Entity{
				Id:      0,
				Weekly:  map[string]int{},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			newWeekly:   map[string]int{"sun 15:30": 30},
			expectedErr: nil,
		},
		{
			testName: "remove 1 from original map",
			entity: &Entity{
				Id:      0,
				Weekly:  map[string]int{"sun 15:30": 30},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			newWeekly:   map[string]int{},
			expectedErr: nil,
		},
		{
			testName: "update duration from original map",
			entity: &Entity{
				Weekly:  map[string]int{"sun 15:30": 30},
				Enabled: true,
			},
			newWeekly:   map[string]int{"sun 15:30": 35},
			expectedErr: nil,
		},
		{
			testName: "bunch of changes",
			entity: &Entity{
				Weekly:  map[string]int{"sun 15:30": 30, "sun 16:00": 35},
				Enabled: true,
			},
			newWeekly:   map[string]int{"sun 15:30": 35, "sun 15:00": 5},
			expectedErr: nil,
		},
	} {

		v.entity.cron = c
		v.entity.cronJobs = map[string]*cron.CronJob{}
		_ = v.entity.cleanAndLoadWeekly()
		_ = v.entity.cleanAndLoadCron()

		err := v.entity.updateWeekly(v.newWeekly)

		if err != nil {
			assert.Equal(t, v.expectedErr, err, fmt.Sprintf("%v expected error: %v got: %v", v.testName, v.expectedErr, err))
			continue
		}

		assert.True(t, areMapsEqual(v.entity.Weekly, v.newWeekly), fmt.Sprintf("%v failed with unequal maps old: %v, expected: %v", v.testName, v.entity.Weekly, v.newWeekly))

		//todo compare cronjobs
	}
}

func TestUpdateCron(t *testing.T) {
	c := cron.New(time.UTC)
	for _, v := range []struct {
		testName string
		entity   *Entity
		newCron  map[string]int
	}{
		{
			testName: "empty",
			entity: &Entity{
				Crons: map[string]int{},
			},
			newCron: map[string]int{},
		},
		{
			testName: "add 1 bad to empty",
			entity: &Entity{
				Crons: map[string]int{},
			},
			newCron: map[string]int{},
		},
		{
			testName: "add 1 good to empty",
			entity: &Entity{
				Crons: map[string]int{},
			},
			newCron: map[string]int{"30 15 * * 0": 30},
		},
		{
			testName: "remove 1 from original map",
			entity: &Entity{
				Crons: map[string]int{"30 15 * * 0": 30},
			},
			newCron: map[string]int{},
		},
		{
			testName: "update duration from original map",
			entity: &Entity{
				Crons: map[string]int{"30 15 * * 0": 30},
			},
			newCron: map[string]int{"30 15 * * 0": 35},
		},
		{
			testName: "bunch of changes",
			entity: &Entity{
				Crons: map[string]int{"30 15 * * 0": 30, "0 16 * * 0": 35},
			},
			newCron: map[string]int{"30 15 * * 0": 35, "0 15 * * 0": 5},
		},
	} {

		v.entity.cron = c
		v.entity.cronJobs = map[string]*cron.CronJob{}
		_ = v.entity.cleanAndLoadWeekly()
		_ = v.entity.cleanAndLoadCron()

		v.entity.updateCrons(v.newCron)

		assert.True(t, areMapsEqual(v.entity.Crons, v.newCron), fmt.Sprintf("%v failed with unequal maps old: %v, expected: %v", v.testName, v.entity.Weekly, v.newCron))

		//todo compare cronjobs
	}
}

func areMapsEqual(a, b map[string]int) bool {
	if len(a) != len(b) {
		return false
	}

	for k, v := range a {
		if v2, ok := b[k]; !ok || v2 != v {
			return false
		}
	}
	return true
}

func TestUpdate(t *testing.T) {
	c := cron.New(time.UTC)
	for _, v := range []struct {
		testName    string
		entity      *Entity
		update      *Entity
		expected    *Entity
		expectedErr error
	}{
		//{
		//	"nil",
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	nil,
		//	nil,
		//	errors.New("update received a nil pointer"),
		//},
		//{
		//	"nil weekly/crons",
		//	&Entity{
		//		Id:      0,
		//		Name:    "old name",
		//		Info:    "old info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   nil,
		//		Weekly:  nil,
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	nil,
		//},
		//{
		//	"bad weekly",
		//	&Entity{
		//		Id:      0,
		//		Name:    "old name",
		//		Info:    "old info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   nil,
		//		Weekly:  map[string]int{"badstring": 30},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	errors.New("bad input to convert to day 'badstring', expected 'sun 15:30' or similar"),
		//},
		//{
		//	"bad weekly",
		//	&Entity{
		//		Id:      0,
		//		Name:    "old name",
		//		Info:    "old info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   nil,
		//		Weekly:  map[string]int{"badstring": 30},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	&Entity{
		//		Id:      0,
		//		Name:    "new name",
		//		Info:    "new info",
		//		Crons:   map[string]int{},
		//		Weekly:  map[string]int{},
		//		Enabled: true,
		//		Next:    "",
		//		NextDur: -1,
		//	},
		//	errors.New("bad input to convert to day 'badstring', expected 'sun 15:30' or similar"),
		//},
		{
			"good weekly",
			&Entity{
				Id:       0,
				Name:     "old name",
				Info:     "old info",
				Crons:    map[string]int{},
				Weekly:   map[string]int{},
				Enabled:  true,
				Next:     "",
				NextDur:  -1,
				cronJobs: map[string]*cron.CronJob{},
			},
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   nil,
				Weekly:  map[string]int{"sun 15:30": 30},
				Enabled: true,
				Next:    "",
				NextDur: -1,
			},
			&Entity{
				Id:      0,
				Name:    "new name",
				Info:    "new info",
				Crons:   map[string]int{},
				Weekly:  map[string]int{"sun 15:30": 30},
				Enabled: true,
				Next:    "sun 15:30",
				NextDur: 30,
			},
			nil,
		},
		//{
		//	"update name",
		//	newPartiallyInitializedEntity(
		//		0, "newName", "", nil, nil, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update info",
		//	newPartiallyInitializedEntity(
		//		0, "", "new-info", nil, nil, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update empty crons",
		//	newPartiallyInitializedEntity(
		//		0, "", "", map[string]int{}, nil, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update one cron",
		//	newPartiallyInitializedEntity(
		//		0, "", "", map[string]int{"0 0 1 1 0": 5}, nil, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update two crons",
		//	newPartiallyInitializedEntity(
		//		0, "", "", map[string]int{"0 0 1 1 0": 5, "0 0 1 2 0": 6}, nil, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update empty weekly",
		//	newPartiallyInitializedEntity(
		//		0, "", "", nil, map[string]int{}, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update one cron",
		//	newPartiallyInitializedEntity(
		//		0, "", "", nil, map[string]int{"fri 05:00": 5}, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update two crons",
		//	newPartiallyInitializedEntity(
		//		0, "", "", nil, map[string]int{"fri 05:00": 5, "mon 05:00": 6}, nil,
		//	),
		//	map[string]int{},
		//	map[string]int{},
		//	nil,
		//},
		//{
		//	"update weekly",
		//	newPartiallyInitializedEntity(
		//		0, "", "", nil, map[string]int{"sat 05:00": 5, "mon 05:00": 6}, nil,
		//	),
		//	nil,
		//	map[string]int{"fri 05:00": 5, "mon 05:00": 7},
		//	nil,
		//},
		//{
		//	"update crons",
		//	newPartiallyInitializedEntity(
		//		0, "", "", map[string]int{"* * * * 2": 5, "* * * * 3": 6}, nil, nil,
		//	),
		//	map[string]int{"* * * * 1": 5, "* * * * 3": 7},
		//	nil,
		//	nil,
		//},
	} {

		v.entity.cron = c
		_ = v.entity.cleanAndLoadWeekly()
		_ = v.entity.cleanAndLoadCron()
		err := v.entity.Update(v.update)

		if err != nil {
			assert.Equal(t, v.expectedErr, err, fmt.Sprintf("%v.%v", v.testName, "expectedErr"))
			continue
		}

		assert.Equal(t, v.expected.Id, v.entity.Id, fmt.Sprintf("%v.%v", v.testName, "id"))
		assert.Equal(t, v.expected.Name, v.entity.Name, fmt.Sprintf("%v.%v", v.testName, "name"))
		assert.Equal(t, v.expected.Status, v.entity.Status, fmt.Sprintf("%v.%v", v.testName, "status"))
		assert.Equal(t, v.expected.Enabled, v.entity.Enabled, fmt.Sprintf("%v.%v", v.testName, "enabled"))
		assert.Equal(t, v.expected.Info, v.entity.Info, fmt.Sprintf("%v.%v", v.testName, "info"))
		assert.Equal(t, v.expected.Weekly, v.entity.Weekly, fmt.Sprintf("%v.%v", v.testName, "weekly"))
		assert.Equal(t, v.expected.Crons, v.entity.Crons, fmt.Sprintf("%v.%v", v.testName, "crons"))
		assert.Equal(t, v.expected.Next, v.entity.Next, fmt.Sprintf("%v.%v", v.testName, "next"))
		assert.Equal(t, v.expected.NextDur, v.entity.NextDur, fmt.Sprintf("%v.%v", v.testName, "nextDur"))

		//todo compare cronjobs
	}
}
