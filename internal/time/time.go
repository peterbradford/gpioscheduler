package time

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"regexp"
	"strings"
	"time"
)

const (
	MainFormat    = "Mon 15:04"
	parseTemplate = "%v %v"
	parseFormat   = "Mon 15:04 2"
)

type Time struct {
	time.Time
}

type Times []time.Time

func (t Times) Len() int {
	return len(t)
}

func (t Times) Less(i, j int) bool {
	return t[i].Before(t[j])
}

func (t Times) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t *Time) UnmarshalJSON(data []byte) error {
	p, err := create(string(data))
	if err != nil {
		return err
	} else {
		*t = *p
	}

	return nil
}

func New(s string) (*Time, error) {
	return create(s)
}

func create(s string) (*Time, error) {
	log.Trace().Msgf("converting '%v' to cron string", s)
	split := strings.Split(strings.Trim(s, "\""), " ")
	if len(split) != 2 {
		err := errors.New("expecting '<day> <hour>:<minute>' got" + s)
		log.Err(err).Msgf("")
		return nil, err
	}

	if match, _ := regexp.MatchString(`^\d{2}:\d{2}$`, split[1]); !match {
		err := errors.New("expecting '<hour>:<minute>' got" + s)
		log.Err(err).Msg("")
		return nil, err
	}

	var day int
	switch strings.ToLower(split[0]) {
	case "sun", "sunday":
		day = int(time.Sunday) // + 2
	case "mon", "monday":
		day = int(time.Monday) // + 2
	case "tue", "tues", "tuesday":
		day = int(time.Tuesday) // + 2
	case "wed", "wednesday":
		day = int(time.Wednesday) // + 2
	case "thu", "thur", "thursday":
		day = int(time.Thursday) // + 2
	case "fri", "friday":
		day = int(time.Friday) // + 2
	case "sat", "saturday":
		day = int(time.Saturday) // + 2
	default:
		err := errors.New(fmt.Sprintf("%v isn't a valid day", split[0]))
		log.Err(err).Msg("")
		return nil, err
	}

	p, err := time.Parse(parseFormat, fmt.Sprintf(parseTemplate, fmt.Sprintf("%v %v", time.Weekday(day).String()[:3], split[1]), day+2))
	if err != nil {
		log.Err(err).Msgf("unable to parse %v into time", fmt.Sprintf("%v %v", strings.Trim(s, "\""), day))
		return nil, err
	}

	return &Time{p}, nil
}
