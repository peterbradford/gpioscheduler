package logger

import (
	"fmt"
	"github.com/rs/zerolog/pkgerrors"
	"io"
	"os"
	"path"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type infoHook struct{}
type fatalHook struct{}

const LogName = "gpioscheduler.log"

// consts logTimeFormat = "02/01/2006-15:04:05.999-MST"
const logTimeFormat = "2006-01-02T15:04:05.000Z07:00"

var (
	zerologLevelsToSentry = map[zerolog.Level]sentry.Level{
		zerolog.TraceLevel: sentry.LevelDebug,
		zerolog.DebugLevel: sentry.LevelDebug,
		zerolog.InfoLevel:  sentry.LevelInfo,
		zerolog.WarnLevel:  sentry.LevelWarning,
		zerolog.ErrorLevel: sentry.LevelError,
		zerolog.FatalLevel: sentry.LevelFatal,
		zerolog.PanicLevel: sentry.LevelFatal,
		zerolog.NoLevel:    sentry.LevelDebug,
		zerolog.Disabled:   sentry.LevelDebug,
	}
	once    sync.Once
	logFile *os.File
)

func Init(level zerolog.Level) {
	//todo setup slog instead of zerolog
	//logger := slog.New(slog.NewJSONHandler(os.Stdout, nil))
	//logger.Info("hello, world", "user", os.Getenv("USER"))

	once.Do(func() {
		zerolog.SetGlobalLevel(level)
		zerolog.TimeFieldFormat = time.RFC3339
		zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
		log.Logger = zerolog.New(os.Stdout).With().Timestamp().Logger() //Caller().Logger()
	})
}

func InitPretty(level zerolog.Level) {
	once.Do(func() {
		zerolog.SetGlobalLevel(level)
		zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
		log.Logger = log.With().Caller().Logger().Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: logTimeFormat, FormatExtra: nil})
	})
}

func CloseLogFile() error {
	if logFile != nil {
		if err := logFile.Close(); err != nil {
			return err
		}
	}
	return nil
}

func InitFile(logPath string) error {
	fullPath := fmt.Sprintf("%s/%s", logPath, LogName)
	if _, err := os.Stat(fullPath); err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(logPath, 0700)
		if err != nil {
			log.Err(err).Msgf("unable to create log dir: %v", logPath)
			return err
		}
	}

	if l, err := os.OpenFile(fullPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0600); err != nil {
		log.Err(err).Msgf("unable to open log file: %v", fullPath)
		return err
	} else {
		log.Logger = log.With().Caller().Logger().Output(io.Writer(l))
		logFile = l
	}

	zerolog.TimeFieldFormat = time.RFC822
	return nil
}

//func AddSentryHook(dsn, commit, env string) {
//	if dsn == "" {
//		log.Warn().Msg("sentry dsn is empty, skipping sentryhook")
//		return
//	}
//	err := sentry.Init(sentry.ClientOptions{
//		Dsn:              dsn,
//		Release:          commit,
//		Environment:      env,
//		AttachStacktrace: true,
//		Debug:            false,
//	})
//
//	if err != nil {
//		log.Err(err).Msg("sentry.init error")
//		return
//	}
//
//	lh := zerolog.LevelHook{
//		//TraceHook:   infoHook{},
//		//DebugHook:   infoHook{},
//		//InfoHook:    infoHook{},
//		WarnHook:  infoHook{},
//		ErrorHook: infoHook{},
//		FatalHook: fatalHook{},
//		PanicHook: fatalHook{},
//		//NoLevelHook: infoHook{},
//	}
//	log.Logger = log.Logger.Hook(lh)
//}

//func (t infoHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
//	buf := getBuf(e)
//	fullMsg := makeMsg(buf, msg)
//	sentry.CaptureEvent(&sentry.Event{
//		Level:   zerologLevelsToSentry[level],
//		Message: fullMsg,
//		Exception: []sentry.Exception{{
//			Type:       fullMsg,
//			Stacktrace: trimStacktrace(sentry.NewStacktrace()),
//		}},
//	})
//}

//func (t fatalHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
//	buf := getBuf(e)
//	fullMsg := makeMsg(buf, msg)
//	sentry.CaptureEvent(&sentry.Event{
//		Level:   zerologLevelsToSentry[level],
//		Message: fullMsg,
//		Exception: []sentry.Exception{{
//			Type:       fullMsg,
//			Stacktrace: trimStacktrace(sentry.NewStacktrace()),
//		}},
//	})
//	sentry.Flush(2 * time.Second)
//}

func getBuf(e *zerolog.Event) string {
	var buf []byte
	fieldvaluelen := reflect.ValueOf(e).Elem().FieldByName("buf")
	if fieldvaluelen.IsValid() && fieldvaluelen.Type().Elem().Kind() == reflect.Uint8 {
		buf = fieldvaluelen.Bytes()
	}
	return string(buf)
}

//func makeMsg(buf, msg string) string {
//	e := gjson.GetAll(buf, `error`)
//	m := strings.TrimSpace(msg)
//	if len(e.Str) > 0 && len(m) > 0 {
//		return m + ": " + e.Str
//	}
//	if len(e.Str) > 0 {
//		return e.Str
//	}
//	return m
//}

func trimStacktrace(stacktrace *sentry.Stacktrace) *sentry.Stacktrace {
	if stacktrace == nil {
		return nil
	}
	filteredFrames := make([]sentry.Frame, 0, len(stacktrace.Frames))

	for _, frame := range stacktrace.Frames {
		// go runtime frames
		if frame.Module == "runtime" || frame.Module == "testing" {
			continue
		}

		if strings.Contains(frame.AbsPath, path.Join("github.com", "rs", "zerolog")) &&
			(strings.Contains(frame.Function, "(*Event).Msg") ||
				strings.Contains(frame.Function, "(*Event).Send")) {
			break
		}
		frame.Filename = path.Join(frame.Module, frame.Filename)
		filteredFrames = append(filteredFrames, frame)
	}
	stacktrace.Frames = filteredFrames
	return stacktrace
}
