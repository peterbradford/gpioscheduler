package config

import (
	"os"
	"time"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

type HardwareType string
type DbType string

const (
	Mqtt HardwareType = "mqtt"
	Rpi               = "rpi"
	HA                = "ha" //home assistant, this would take some big work to get working
	Test              = "test"

	File     DbType = "file"
	Postgres        = "pg"
	SQLite          = "sqlite"
)

type Config struct {
	Location     *time.Location
	HardwareType HardwareType
	MQTT         MQTT

	LogLevel int
	LogPath  string

	BackendEndpoint  string
	FrontendEndpoint string
	StaticPath       string
	DBType           DbType
	DBPath           string
}

type MQTT struct {
	User     string
	Password string
	Topic    string
	Host     string
	ClientID string
}

func InitConfig() *Config {
	if err := godotenv.Load(".env"); err != nil {
		log.Warn().Msg("no .env file loaded, using env vars only")
	}

	v := viper.New()

	//defaults
	v.SetDefault("LOCATION", time.UTC)
	v.SetDefault("HARDWARE_TYPE", Test)
	v.SetDefault("LOG_LEVEL", 1)
	v.SetDefault("LOG_PATH", "") //leave empty unless you want it to log to a file
	v.SetDefault("BACKEND_ENDPOINT", "0.0.0.0:10101")
	v.SetDefault("FRONTEND_ENDPOINT", "0.0.0.0:8099")
	v.SetDefault("DB_PATH", "resources")
	v.SetDefault("DB_TYPE", string(File))
	v.SetDefault("STATIC_PATH", "resources/static")
	v.SetDefault("MQTT_CLIENT_ID", "scheduler")

	v.AutomaticEnv()

	c := new(Config)

	c.HardwareType = HardwareType(v.GetString("HARDWARE_TYPE"))
	if c.HardwareType == Mqtt {
		c.MQTT.User = v.GetString("MQTT_USER")         //not required
		c.MQTT.Password = v.GetString("MQTT_PASSWORD") //not required
		c.MQTT.Topic = v.GetString("MQTT_TOPIC")       //required
		c.MQTT.Host = v.GetString("MQTT_HOST")         //required
	}
	c.LogLevel = v.GetInt("LOG_LEVEL")
	c.LogPath = v.GetString("LOG_PATH")
	c.BackendEndpoint = v.GetString("BACKEND_ENDPOINT")
	c.FrontendEndpoint = v.GetString("FRONTEND_ENDPOINT")
	c.StaticPath = v.GetString("STATIC_PATH")
	c.DBPath = v.GetString("DB_PATH")
	c.DBType = DbType(v.GetString("DB_TYPE"))
	if c.DBType == Postgres {
		log.Warn().Msg("dbtype set to postgres")
	}

	if loc, err := time.LoadLocation(v.GetString("LOCATION")); err == nil {
		c.Location = loc
	} else {
		log.Err(err).Msg("error parsing location, using default UTC")
		c.Location = time.UTC
	}

	if err := c.Validate(); err != nil {
		tmp := log.With().Caller().Logger().Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
		tmp.Err(err).Msg("")
		return nil
	}

	return c
}

func (c Config) Validate() error {
	return validation.ValidateStruct(&c) //validation.Field(&c.Api),
}
