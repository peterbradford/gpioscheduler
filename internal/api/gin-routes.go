package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (s *Service) NewGinHandler() http.Handler {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(cors.Default(), gin.Recovery()) //, rscors.AllowAll())

	r.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, "ok")
	})

	pins := r.Group("/pins").Use(gin.Logger()).Use(timeoutMiddleware())

	pins.GET("", s.getPinsGin)
	pins.GET("/:pin", s.getPinGin)
	pins.GET("/remaining/:pin", s.getRemainingRunTimeGin)

	pins.PUT("", s.updatePinGin)
	pins.PUT("/on/:pin", s.turnOnPinGin)
	pins.PUT("/off/:pin", s.turnOffPinGin)
	pins.PUT("/enable/:pin", s.enablePinGin)
	pins.PUT("/disable/:pin", s.disablePinGin)
	pins.PUT("/run/:pin/:dur", s.runOneOffGin)

	pins.POST("", s.createPinGin)

	pins.DELETE("/:pin", s.deletePinGin)

	cron := r.Group("/cron").Use(gin.Logger()).Use(timeoutMiddleware())

	cron.GET("", s.getCronJobsGin)

	return r
}

func (s *Service) NewFrontEndGinHandler() http.Handler {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(cors.Default(), gin.Recovery()) //, showHeadersMiddleware()) //, rscors.AllowAll())

	r.Use(static.Serve("/", static.LocalFile(s.config.StaticPath, true)))

	return r
}
