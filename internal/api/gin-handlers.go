package api

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

type ErrorMessage struct {
	ErrorMessages []string `json:"error"`
}

func (s *Service) getPinsGin(c *gin.Context) {
	//c.JSON(http.StatusOK, s.entityManager.GetAll())
}

func (s *Service) getPinGin(c *gin.Context) {
	if id, err := s.validatePinParam(c.Param("pin")); err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}})
	} else {
		c.JSON(http.StatusOK, s.entityManager.Get(id))
	}
}

func (s *Service) getRemainingRunTimeGin(c *gin.Context) {
	if id, err := s.validatePinParam(c.Param("pin")); err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}})
	} else {
		runningStats := s.entityManager.GetRemainingRunTime(c, id)
		c.JSON(http.StatusOK, runningStats)
	}
}

func (s *Service) updatePinGin(c *gin.Context) {
	rtnCode, rtnMsg := s.entityManager.Update(c.Request.Body)
	c.JSON(rtnCode, rtnMsg)
}

func (s *Service) turnOnPinGin(c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", c.Param("pin"))
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.TurnOn(id)
		c.JSON(http.StatusOK, "")
	}
}

func (s *Service) turnOffPinGin(c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", c.Param("pin"))
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.TurnOff(id)
		c.JSON(http.StatusOK, "")
	}
}

func (s *Service) enablePinGin(c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", c.Param("pin"))
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.Enable(id)
		c.JSON(http.StatusOK, "")
	}
}

func (s *Service) disablePinGin(c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", c.Param("pin"))
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.Disable(id)
		c.JSON(http.StatusOK, "")
	}
}

func (s *Service) runOneOffGin(c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", c.Param("pin"))
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else if dur, err := strconv.Atoi(c.Param("dur")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", c.Param("dur"))
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading duration: %v", id), err.Error()}})
	} else {
		s.entityManager.RunOneOff(id, dur)
		c.JSON(http.StatusOK, "")
	}
}

func (s *Service) createPinGin(c *gin.Context) {

	statusCode, rtnMsg := s.entityManager.Create(c.Request.Body)
	c.JSON(statusCode, rtnMsg)
}

func (s *Service) deletePinGin(c *gin.Context) {
	if id, err := s.validatePinParam(c.Param("pin")); err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}})
	} else {
		rtnCode, rtnMsg := s.entityManager.Delete(id)
		c.JSON(rtnCode, rtnMsg)
	}
}

func (s *Service) getCronJobsGin(c *gin.Context) {
	c.JSON(http.StatusOK, s.entityManager.GetCronJobs())
}

func (s *Service) validatePinParam(n string) (int, error) {
	if id, err := strconv.Atoi(n); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", n)
		return -1, err
	} else {
		return id, nil
	}
}
