package api

import (
	"net/http"
)

func (s *Service) NewBackEndHandler() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("GET /health", s.getHealthHandler)

	//pin related calls
	mux.HandleFunc("GET /pins", s.getPinsHandler)

	mux.HandleFunc("GET /pins/{pin}", s.getPinHandler)

	mux.HandleFunc("GET /pins/remaining/{pin}", s.getRemainingRunTimeHandler)

	mux.HandleFunc("PUT /pins/on/{pin}", s.turnOnPinHandler)

	mux.HandleFunc("PUT /pins/off/{pin}", s.turnOffPinHandler)

	mux.HandleFunc("PUT /pins/enable/{pin}", s.enablePinHandler)

	mux.HandleFunc("PUT /pins/disable/{pin}", s.disablePinHandler)

	mux.HandleFunc("PUT /pins", s.updatePinHandler)

	mux.HandleFunc("PUT /pins/run/{pin}/{dur}", s.runOneOffHandler)

	mux.HandleFunc("POST /pins", s.createPinHandler)

	mux.HandleFunc("DELETE /pins/{pin}", s.deletePinHandler)

	//cron related calls
	mux.HandleFunc("GET /cron", s.getCronJobsHandler)

	//test function, should handle ONLY the "/"
	//mux.HandleFunc("/{$}", func(w http.ResponseWriter, r *http.Request) {
	//	w.Write([]byte("handle \"/\" only"))
	//})
	//
	//mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	//	w.Write([]byte("catch all"))
	//})

	return mux
}

func (s *Service) NewFrontEndHandler() http.Handler {
	mux := http.NewServeMux()

	//todo look into abusing http.dir file servers via file path traversal bug https://owasp.org/www-community/attacks/Path_Traversal
	fs := http.FileServer(http.Dir("./" + s.config.StaticPath))
	mux.Handle("/", fs)

	return mux
}
