package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/rs/zerolog/log"
)

func writeJson(w http.ResponseWriter, header int, v any) {
	b, err := json.Marshal(v)
	if err != nil {
		//todo catch err
	}
	w.WriteHeader(header)
	_, err = w.Write(b)
	if err != nil {
		//todo catch err
	}
}

func (s *Service) getHealthHandler(w http.ResponseWriter, r *http.Request) {
	writeJson(w, http.StatusOK, "ok")
}

func (s *Service) getPinsHandler(w http.ResponseWriter, r *http.Request) {
	writeJson(w, http.StatusOK, s.entityManager.GetAll())
}

func (s *Service) getPinHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := s.validatePinParam(r.PathValue("pin")); err != nil {
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}})
	} else {
		writeJson(w, http.StatusOK, s.entityManager.Get(id))
	}
}

func (s *Service) getRemainingRunTimeHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := s.validatePinParam(r.PathValue("pin")); err != nil {
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}})
	} else {
		runningStats := s.entityManager.GetRemainingRunTime(r.Context(), id)
		writeJson(w, http.StatusOK, runningStats)
	}
}

func (s *Service) updatePinHandler(w http.ResponseWriter, r *http.Request) {
	rtnCode, rtnMsg := s.entityManager.Update(r.Body)
	writeJson(w, rtnCode, rtnMsg)
}

func (s *Service) turnOnPinHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := strconv.Atoi(r.PathValue("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", r.PathValue("pin"))
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.TurnOn(id)
		writeJson(w, http.StatusOK, "")
	}
}

func (s *Service) turnOffPinHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := strconv.Atoi(r.PathValue("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", r.PathValue("pin"))
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.TurnOff(id)
		writeJson(w, http.StatusOK, "")
	}
}

func (s *Service) enablePinHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := strconv.Atoi(r.PathValue("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", r.PathValue("pin"))
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.Enable(id)
		writeJson(w, http.StatusOK, "")
	}
}

func (s *Service) disablePinHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := strconv.Atoi(r.PathValue("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", r.PathValue("pin"))
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else {
		s.entityManager.Disable(id)
		writeJson(w, http.StatusOK, "")
	}
}

func (s *Service) runOneOffHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := strconv.Atoi(r.PathValue("pin")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", r.PathValue("pin"))
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading pin number: %v", id), err.Error()}})
	} else if dur, err := strconv.Atoi(r.PathValue("dur")); err != nil {
		log.Error().Err(err).Msgf("invalid parameter: %v", r.PathValue("dur"))
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("issue reading duration: %v", id), err.Error()}})
	} else {
		s.entityManager.RunOneOff(id, dur)
		writeJson(w, http.StatusOK, "")
	}
}

func (s *Service) createPinHandler(w http.ResponseWriter, r *http.Request) {
	statusCode, rtnMsg := s.entityManager.Create(r.Body)
	writeJson(w, statusCode, rtnMsg)
}

func (s *Service) deletePinHandler(w http.ResponseWriter, r *http.Request) {
	if id, err := s.validatePinParam(r.PathValue("pin")); err != nil {
		writeJson(w, http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}})
	} else {
		rtnCode, rtnMsg := s.entityManager.Delete(id)
		writeJson(w, rtnCode, rtnMsg)
	}
}

func (s *Service) getCronJobsHandler(w http.ResponseWriter, r *http.Request) {
	writeJson(w, http.StatusOK, s.entityManager.GetCronJobs())
}
