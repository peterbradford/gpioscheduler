package api

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/peterbradford/gpioscheduler/internal/config"
	"gitlab.com/peterbradford/gpioscheduler/internal/entitymanager"
)

type Service struct {
	wg            *sync.WaitGroup
	config        *config.Config
	errChan       chan error
	backendSrv    *http.Server
	frontendSrv   *http.Server
	entityManager *entitymanager.Service
}

func New(wg *sync.WaitGroup, cf *config.Config, errChan chan error, em *entitymanager.Service) *Service {
	s := &Service{
		wg:      wg,
		errChan: errChan,
		config:  cf,

		backendSrv:  &http.Server{Addr: cf.BackendEndpoint},
		frontendSrv: &http.Server{Addr: cf.FrontendEndpoint},

		entityManager: em,
	}

	s.backendSrv.Handler = s.NewBackEndHandler()
	s.frontendSrv.Handler = s.NewFrontEndHandler()

	return s
}

func (s *Service) Run() {
	go func() {
		s.wg.Add(1)
		defer s.wg.Done()
		log.Info().Msgf("frontend running on: %v", s.frontendSrv.Addr)
		if err := s.frontendSrv.ListenAndServe(); err != nil && err.Error() != "http: Server closed" {
			log.Err(err).Msgf("frontend server failed")
			s.errChan <- fmt.Errorf("frontend server failed: %v", err)
			close(s.errChan)
		}
	}()

	go func() {
		s.wg.Add(1)
		defer s.wg.Done()
		log.Info().Msgf("backend running on: %v", s.backendSrv.Addr)
		if err := s.backendSrv.ListenAndServe(); err != nil && err.Error() != "http: Server closed" {
			log.Err(err).Msgf("backend server failed")
			s.errChan <- fmt.Errorf("backend server failed: %v", err)
			close(s.errChan)
		}
	}()
}

func (s *Service) Stop() {
	log.Info().Msg("shutting down servers")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if err := s.backendSrv.Shutdown(ctx); err != nil {
		log.Err(err).Msg("error while stopping backend server")
		s.errChan <- fmt.Errorf("error while stopping backend server: %v", err)
	}

	if err := s.frontendSrv.Shutdown(ctx); err != nil {
		log.Err(err).Msg("error while stopping frontend server")
		s.errChan <- fmt.Errorf("error while stopping frontend server: %v", err)
	}
	close(s.errChan)
}
