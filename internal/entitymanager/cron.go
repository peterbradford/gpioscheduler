package entitymanager

import (
	"gitlab.com/peterbradford/gpioscheduler/internal/cron"
)

func (s *Service) GetCronJobs() []cron.Job {
	return s.cron.GetCronJobs()
}
