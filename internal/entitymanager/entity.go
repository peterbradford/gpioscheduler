package entitymanager

import (
	"context"
	"fmt"
	"io"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/peterbradford/gpioscheduler/internal/entity"
)

func (s *Service) addEntity(e *entity.Entity) error {
	if newE, err := entity.New(e.Id, e.Name, e.Info, e.Crons, e.Weekly, s.hardwareHandler, s.cron); err != nil {
		log.Err(err).Msgf("unable to add entity %v", *e)
		return err
	} else {
		//todo checkIfEntityAlreadyExists

		s.entities[e.Id] = newE

		err := s.db.UpdateAll(s.entities)
		if err != nil {
			return err
		}
		return nil
	}
}

func (s *Service) Create(body io.ReadCloser) (int, interface{}) {
	e, err := s.decodeEntity(body)
	if err != nil {
		return http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}}
	}

	if _, ok := s.entityExists(e.Id); ok {
		log.Warn().Msgf("unable to add entity with id %v since it already exists", e.Id)
		return http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("entity with id %v already exists", e.Id)}}
	}

	if err = s.addEntity(e); err != nil {
		//todo log
		return http.StatusInternalServerError, ErrorMessage{ErrorMessages: []string{err.Error()}}
	} else {
		return http.StatusOK, ""
	}
}

func (s *Service) Delete(id int) (int, interface{}) {
	if _, ok := s.entityExists(id); !ok {
		return http.StatusInternalServerError, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("entity with id %v doesn't exist", id)}}
	}

	if err := s.deleteEntity(id); err != nil {
		return http.StatusInternalServerError, ErrorMessage{ErrorMessages: []string{err.Error()}}
	} else {
		return http.StatusOK, ""
	}
}

func (s *Service) Update(body io.ReadCloser) (int, interface{}) {
	e, err := s.decodeEntity(body)
	if err != nil {
		//todo log
		return http.StatusBadRequest, ErrorMessage{ErrorMessages: []string{err.Error()}}
	}

	if _, ok := s.entityExists(e.Id); !ok {
		return http.StatusInternalServerError, ErrorMessage{ErrorMessages: []string{fmt.Sprintf("entity with id %v doesn't exist", e.Id)}}
	}

	if err := s.updateEntity(e); err != nil {
		return http.StatusInternalServerError, ErrorMessage{ErrorMessages: []string{err.Error()}}
	} else {
		return http.StatusOK, ""
	}
}

func (s *Service) Get(id int) *entity.Entity {
	//todo is it nil if it doesn't exist?
	return s.entities[id]
}

func (s *Service) GetAll() []*entity.Entity {
	var entities []*entity.Entity
	for _, v := range s.entities {
		entities = append(entities, v)
	}
	return entities
}

func (s *Service) TurnOn(id int) {
	if _, ok := s.entities[id]; ok && s.entities[id].Enabled {
		log.Debug().Msgf("turning entity %v on", id)
		s.entities[id].TurnOn()
		//s.hardwareHandler.TurnOn(id)
		s.db.UpdateAll(s.entities)
	}
}

func (s *Service) TurnOff(id int) {
	if _, ok := s.entities[id]; ok {
		log.Debug().Msgf("turning entity %v off", id)
		s.entities[id].Status = false
		s.entities[id].TurnOff()
		//s.hardwareHandler.TurnOff(id)
		s.db.UpdateAll(s.entities)
	}
}

func (s *Service) Enable(id int) {
	if _, ok := s.entities[id]; ok {
		log.Debug().Msgf("enabling entity %v", id)
		s.entities[id].Enable()
		s.db.UpdateAll(s.entities)
	}
}

func (s *Service) Disable(id int) {
	if _, ok := s.entities[id]; ok {
		log.Debug().Msgf("disabling entity %v", id)
		s.entities[id].Disable()
		s.db.UpdateAll(s.entities)
	}
}

func (s *Service) RunOneOff(id, dur int) {
	go s.entities[id].RunEntityForDurFuncCreator(dur)
}

func (s *Service) GetRemainingRunTime(ctx context.Context, id int) *entity.RunningStats {
	if _, ok := s.entities[id]; ok {
		return s.entities[id].GetRemainingTime(ctx)
	} else {
		return nil
	}
}
