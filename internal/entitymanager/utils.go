package entitymanager

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/rs/zerolog/log"

	"gitlab.com/peterbradford/gpioscheduler/internal/entity"
)

type ErrorMessage struct {
	ErrorMessages []string `json:"error"`
}

func (s *Service) decodeEntity(closer io.ReadCloser) (*entity.Entity, error) {
	e := &entity.Entity{}
	if err := json.NewDecoder(closer).Decode(e); err != nil {
		log.Error().Err(err).Msgf("unable to decode entity: %v", err)
		return nil, err
	} else {
		return e, nil
	}
}

func (s *Service) checkIfEntityAlreadyExists(e *entity.Entity) error {
	if _, ok := s.entities[e.Id]; ok {
		err := fmt.Errorf("unable to add entity, id %v already exists", e.Id)
		return err
	}
	return nil
}

func (s *Service) deleteEntity(id int) error {
	s.entities[id].RemoveAllCronJobs()
	delete(s.entities, id)
	if err := s.db.UpdateAll(s.entities); err != nil {
		return err
	} else {
		return nil
	}
}

func (s *Service) updateEntity(e *entity.Entity) error {
	if err := s.entities[e.Id].Update(e); err != nil {
		return err
	} else {
		if err = s.db.UpdateAll(s.entities); err != nil {
			return err
		}
		return err
	}
}

func (s *Service) entityExists(id int) (*entity.Entity, bool) {
	e, ok := s.entities[id]
	return e, ok
}
