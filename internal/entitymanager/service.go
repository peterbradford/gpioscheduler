package entitymanager

import (
	"net/http"

	"github.com/rs/zerolog/log"

	"gitlab.com/peterbradford/gpioscheduler/internal/config"
	"gitlab.com/peterbradford/gpioscheduler/internal/cron"
	"gitlab.com/peterbradford/gpioscheduler/internal/db"
	"gitlab.com/peterbradford/gpioscheduler/internal/entity"
	"gitlab.com/peterbradford/gpioscheduler/internal/hardware"
)

type Service struct {
	config          *config.Config
	errChan         chan error
	hardwareHandler hardware.Hardware
	srv             *http.Server
	frontEndSrv     *http.Server
	entities        map[int]*entity.Entity
	db              db.DB
	cron            *cron.Cron
}

func New(cf *config.Config, errChan chan error, hardwareHandler hardware.Hardware, db db.DB, c *cron.Cron) *Service {
	s := &Service{
		errChan:         errChan,
		config:          cf,
		hardwareHandler: hardwareHandler,
		entities:        make(map[int]*entity.Entity),

		srv:         &http.Server{Addr: cf.BackendEndpoint},
		frontEndSrv: &http.Server{Addr: cf.FrontendEndpoint},
		db:          db,
		cron:        c,
	}

	entities, err := s.db.GetAll()
	if err != nil {
		s.errChan <- err
		close(s.errChan)
		return nil
	}

	for _, v := range entities {
		v.Status = false

		if newE, err := entity.New(v.Id, v.Name, v.Info, v.Crons, v.Weekly, s.hardwareHandler, c); err != nil {
			log.Err(err).Msgf("error creating entity %v", v)
			s.errChan <- err
			close(s.errChan)
			return nil
		} else {
			s.entities[v.Id] = newE
			s.entities[v.Id].TurnOff()
		}
	}

	if err = s.db.UpdateAll(s.entities); err != nil {
		s.errChan <- err
		close(s.errChan)
		return nil
	}

	return s
}

func (s *Service) Stop() {
	log.Info().Msg("stopping")

	for _, p := range s.entities {
		s.hardwareHandler.TurnOff(p.Id)
	}
}
