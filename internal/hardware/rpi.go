package hardware

import (
	"github.com/rs/zerolog/log"
	"github.com/stianeikeland/go-rpio"
	"gitlab.com/peterbradford/gpioscheduler/internal/config"
)

var PhysicalPins = map[int]int{0: 4, 1: 5, 2: 6, 3: 12, 4: 13, 5: 16, 6: 17, 7: 18, 8: 19, 9: 20, 10: 21, 11: 22, 12: 23, 13: 24, 14: 25, 15: 27}

type Rpi struct{}

func NewRpi(conf *config.Config) (*Rpi, error) {
	return &Rpi{}, nil
}

func (r Rpi) TurnOn(pin int) {
	if err := rpio.Open(); err != nil {
		log.Warn().Msg("unable to access gpio pins")
		return
	}
	if _, ok := PhysicalPins[pin]; !ok {
		log.Warn().Msgf("pin %v isn't a valid pinNum", pin)
		return
	}
	log.Debug().Msgf("turning pin %v on", pin)
	physicalPin := rpio.Pin(PhysicalPins[pin])
	physicalPin.Output()
	physicalPin.High()
}

func (r Rpi) TurnOff(pin int) {
	if err := rpio.Open(); err != nil {
		log.Warn().Msg("unable to access gpio pins")
		return
	}
	if _, ok := PhysicalPins[pin]; !ok {
		log.Warn().Msgf("pin %v isn't a valid pinNum", pin)
		return
	}
	log.Debug().Msgf("turning pin %v off", pin)
	physicalPin := rpio.Pin(PhysicalPins[pin])
	physicalPin.Output()
	physicalPin.Low()
}
