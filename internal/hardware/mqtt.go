package hardware

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"gitlab.com/peterbradford/gpioscheduler/internal/config"
	"net/url"
	"time"
)

const MqttTopicTemplate = `{"id": %v}`

type MQTT struct {
	topic string
	c     mqtt.Client
}

func NewMQTT(conf *config.Config) (*MQTT, error) {
	opts := createClientOptions(conf.MQTT.ClientID, &url.URL{Host: conf.MQTT.Host, User: url.UserPassword(conf.MQTT.User, conf.MQTT.Password)})
	c := mqtt.NewClient(opts)
	token := c.Connect()
	token.WaitTimeout(3 * time.Second)

	if err := token.Error(); err != nil {
		log.Error().Msgf("unable to get token for mqtt: %v", err)
		return nil, err
	}

	if !c.IsConnected() {
		log.Error().Msgf("mqtt not connected")
		return nil, fmt.Errorf("mqtt not connected")
	}

	return &MQTT{
		topic: conf.MQTT.Topic,
		c:     c,
	}, nil
}

func createClientOptions(clientId string, uri *url.URL) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))
	opts.SetUsername(uri.User.Username())
	if password, set := uri.User.Password(); set {
		opts.SetPassword(password)
	}
	opts.SetClientID(clientId)
	return opts
}

func (m MQTT) TurnOn(id int) {
	log.Debug().Msgf("sending 'turn on' command to MQTT topic '%v/on' with payload %v", m.topic, fmt.Sprintf(MqttTopicTemplate, id))
	t := m.c.Publish(m.topic+"/on", 0, false, fmt.Sprintf(MqttTopicTemplate, id))
	_ = t.WaitTimeout(time.Second * 3)
	if t.Error() != nil {
		log.Err(t.Error()).Msg("error while publishing msg")
	}
}

func (m MQTT) TurnOff(id int) {
	log.Debug().Msgf("sending 'turn off' command to MQTT topic '%v/off' with payload %v", m.topic, fmt.Sprintf(MqttTopicTemplate, id))
	t := m.c.Publish(m.topic+"/off", 0, false, fmt.Sprintf(MqttTopicTemplate, id))
	_ = t.WaitTimeout(time.Second * 3)
	if t.Error() != nil {
		log.Err(t.Error()).Msg("error while publishing msg")
	}
}
