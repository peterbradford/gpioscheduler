package hardware

import (
	"github.com/rs/zerolog/log"
	//"gitlab.com/peterbradford/gpioscheduler/internal/pin"
)

type Tester struct {
}

func NewTester() *Tester {
	return &Tester{}
}

func (m *Tester) TurnOn(i int) {
	log.Debug().Msgf("sending 'turn on' command to tester for id %v", i)
}

func (m *Tester) TurnOff(i int) {
	log.Debug().Msgf("sending 'turn off' command to tester for id %v", i)
}
