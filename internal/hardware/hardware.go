package hardware

type Hardware interface {
	TurnOn(int)
	TurnOff(int)
}
