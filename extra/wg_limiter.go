package main

import (
	"fmt"
	"sync"
	"time"
)

type Item struct {
	id int
}

func main() {
	items := []Item{
		{id: 1},
		{id: 2},
		{id: 3},
		{id: 4},
		{id: 5},
		{id: 6},
		{id: 7},
		{id: 8},
		{id: 9},
		{id: 10},
	}

	start := time.Now()
	sema := make(chan struct{}, 10)
	wg := sync.WaitGroup{}
	for _, item := range items {
		// acquire token
		sema <- struct{}{}
		wg.Add(1)
		go func(item Item) {
			// do job
			time.Sleep(time.Second * 5)
			fmt.Println("id: ", item.id)
			// release semaphone
			<-sema
			wg.Done()
		}(item)
	}
	wg.Wait()
	fmt.Println("end: ", time.Now().Sub(start))
}
