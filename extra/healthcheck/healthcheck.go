package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	//args := os.Args[1:]
	//if len(args) == 0 {
	//	fmt.Println("not enough args")
	//	os.Exit(1)
	//}
	//_, err := http.GetAll(fmt.Sprintf("http://127.0.0.1:%s/health", args[0]))
	fmt.Println("making request to 127.0.0.1:8082")
	_, err := http.Get(fmt.Sprintf("http://127.0.0.1:8082/health"))
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	fmt.Println("making request to localhost:8082")
	_, err = http.Get(fmt.Sprintf("http://localhost:8082/health"))
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}
