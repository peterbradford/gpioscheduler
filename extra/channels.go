package main

import (
	"fmt"
	"time"
)

//func write(write chan string, stop chan interface{}) {
//stp:
//	for {
//		select {
//		case <- stop:
//			fmt.Println("stopping writer")
//			close(write)
//			break stp
//		default:
//			fmt.Println(len(write))
//			fmt.Println(cap(write))
//			fmt.Println("writing")
//			write <- "stuff"
//			time.Sleep(time.Millisecond * 500)
//		}
//	}
//	fmt.Println("Stopped writer")
//}
//
//func read(read chan string, stop chan interface{}) {
//	//stp:
//	for {
//		select {
//		//case <- stop:
//		//	fmt.Println("stopping")
//		//break stp
//		//fmt.Printf("got message: %v\n", msg)
//		//time.Sleep(time.Millisecond * 200)
//		case msg := <- read:
//			fmt.Println("msg read:", msg)
//		default:
//			time.Sleep(time.Millisecond * 500)
//			fmt.Println("reader is still reading")
//		}
//	}
//}
//
//func main() {
//	chan1 := make(chan string, 4)
//	stop := make(chan interface{})
//	go write(chan1, stop)
//	go read(chan1, stop)
//	time.Sleep(2 * time.Second)
//	stop <- ""
//	fmt.Println("closing")
//	time.Sleep(50 * time.Millisecond)
//
//	msg := <- chan1
//	fmt.Println("read a message", msg)
//	//msg = <- chan1
//	//fmt.Println("read a message", msg)
//	//msg = <- chan1
//	//fmt.Println("read a message", msg)
//	//msg = <- chan1
//	//fmt.Println("read a message", msg)
//	//msg = <- chan1
//	//fmt.Println("read a message", msg)
//}

//channels that were created with no buffers specified have to have a reader
//channels that have a buffer specified can only be written to to the buffer
//	amount and if another read is attempted it will deadlock
//channels that are closed but a reader was attached and not stopped will
//	cause a large amount of empty messages to be read until the program is
//	closed
//easiest way to avoid this is by adding a closed check to the reader with 'msg, closed := <-channel'
//	this works for multiple readers
//for multiple writers, can use a wg to close properly and the channels will need to be wrapped in a struct that
//	holds the wg and read/write/close function.
//https://www.leolara.me/blog/closing_a_go_channel_written_by_several_goroutines/
//a second option is to use an additional close channel for all writers

//all in all, would it not be easier to catch the panic from the send on closed channel?

func read(read chan string) {
	for {
		select {
		case j, more := <-read:
			if more {
				fmt.Println("received", j)
			} else {
				fmt.Println("chan closed")
				return
			}
		}
	}
}

func write(write chan string, done chan bool) {
	for {
		select {
		case <-done:
			close(write)
			return
		default:
			time.Sleep(time.Millisecond * 500)
			write <- "message"
		}
	}
}

func main() {
	//jobs := make(chan string, 5)
	//done := make(chan bool)
	//go read(jobs)
	//go write(jobs, done)
	//go write(jobs, done)
	//go write(jobs, done)
	//go write(jobs, done)
	//go write(jobs, done)
	//time.Sleep(time.Second * 2)
	//done <- true
	//time.Sleep(time.Second * 2)

	jobs := make(chan int)

	go func() {
		for {
			r, ok := <-jobs
			if ok {
				fmt.Println(r)
			} else {
				fmt.Println("closed")
				time.Sleep(time.Second * 1)
			}
		}
	}()

	jobs <- 1

	close(jobs)

	//jobs <- 1

}

//channels that were created with no buffers specified have to have a reader
//channels that have a buffer specified can only be written to up to the buffer size
//  amount and if another read is attempted it will deadlock
//channels that are closed but a reader was attached and not stopped will
//	cause a large amount of empty messages to be read until the program is
//	closed
//readers can use the second read variable to check if channel is closed
//writers close the channel as they panic if they write to a closed channel.
//https://www.leolara.me/blog/closing_a_go_channel_written_by_several_goroutines/
//https://go101.org/article/channel-closing.html
